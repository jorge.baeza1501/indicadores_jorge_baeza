const fs= require('fs');
const timestamp= require('time-stamp');

const escribirDatos= (datos)=>{
    return new Promise((resolve,reject)=>{
        var n=0;
        var path='datos/';
        fs.readdir(path,(err,files)=>{
            n=files.length+1;
            if(n<9){
                n='0'+n;
            }
            fs.writeFile("datos/"+n+String(timestamp('_YYYY-MM-DD-HH-mm-ss'))+'.ind',JSON.stringify(datos),(err)=>{
                console.error(err)});
        });

    })
}

const getFile = fileName => {
    return new Promise((resolve, reject) => {
        fs.readFile(fileName, (err, data) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(data);
        });
    });
}

exports.escribirDatos= escribirDatos;
exports.getFile= getFile;