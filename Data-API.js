const fetch = require('node-fetch');
const timestamp= require('time-stamp');

const obtenerDatosAPI = () =>{
    return new Promise((resolve, reject)=>{
        resolve(fetch('https://mindicador.cl/api').then(status).then(obtenerJson).then(filtrado));
    });
}

const status = response =>{
    if(response.status >= 200 && response.status < 300){
        return Promise.resolve(response);
    }
    return Promise.reject(new Error(response.statusText));
};

const obtenerJson = response => {
    return response.json();
};

const filtrado = (datos) => {
    var cont = 0;
    var fecha = 'fecha';
    var fecha2 = String(timestamp('DD-MM-YYYY'));
    var hora = 'hora';
    var hora2 = String(timestamp('HH:mm:ss'));
    var info = '{'+JSON.stringify(fecha)+':'+JSON.stringify(fecha2)+','+JSON.stringify(hora)+':'+JSON.stringify(hora2)+',';
    for(var elem in datos){
        if(elem === 'dolar' || elem === 'euro' || elem === 'tasa_desempleo'){
            cont++;
            if(cont==1 || cont==2){
                info+=JSON.stringify(elem)+":"+JSON.stringify(datos[elem])+",";
            }else{
                info+=JSON.stringify(elem)+":"+JSON.stringify(datos[elem])+"}";
            }
        }
    }
    console.log(info);
    return Promise.resolve(JSON.parse(info));
};

exports.obtenerDatosAPI=obtenerDatosAPI;