const fetch = require('node-fetch');
const timestamp= require('time-stamp');
const readline = require('readline');
const fs = require ('fs');
const datosAPI = require('./Data-API');
const escribeFil = require('./Files-API');
var arregloFechas = [];
var arregloFechas2 = [];
var arregloValores = [];
var minimoD=999999999;
var minimoE=999999999;
var minimoTD=999999999;
var arregloFechas3 = [];
var arregloValores2 = [];
var maximoD=0;
var maximoE=0;
var maximoTD=0;

var lector = readline.createInterface({
	input: process.stdin,
	output: process.stdout
});

var leerOpcionMenu = (lectura) =>{
    return new Promise((resolve, reject)=>{
        console.log("\nMENU");
        console.log("1. Actualizar indicadores");
        console.log("2. Promediar");
        console.log("3. Mostrar el valor más actual");
        console.log("4. Mostrar mínimo histórico");
        console.log("5. Mostrar máximo histórico");
        console.log("6. Salir");
        lectura.question('\nEscriba una una opción: ', opcion =>{
            resolve(opcion);
        });
    });
}

var leerOpcionCaso2 = (lectura) =>{
    return new Promise((resolve, reject)=>{
        console.log("\nElija indicador a promediar:");
        console.log("1. Dólar");
        console.log("2. Euro");
        console.log("3. Tasa de desempleo");
        console.log("4. Regresar al menú principal");
        lectura.question('\nEscriba una una opción: ', opcion =>{
            resolve(opcion);
        });
    });
}

//

leerOpcionMenu(lector).then((opcion)=>{menu(opcion);}).catch(console.error);
const menu = (opcion) => {
		switch(opcion){
			case '1':
				caso1();
				break;
			case '2':
				leerOpcionCaso2(lector).then((opcion)=>{caso2(opcion);}).catch(console.error);
				break;
			case '3':
                caso3().then(console.log);
                break;
            case '4':
                caso4().then(calcularRangos).then(console.log);
                break;
            case '5':
                caso5().then(calcularRangos5).then(console.log);
                break;        
			case '6':
				lector.close();
				process.exit(0);
				break;
			default:
				console.log('Opción no encontrada');
				leerOpcionMenu(lector).then(menu);
				break;
		}   
}

const caso1 = () =>{
        datosAPI.obtenerDatosAPI().then((datos)=>{
            escribeFil.escribirDatos(datos)
       }).catch(console.error);    
}

const caso2 = (opcion) =>{
		switch(opcion){
			case '1':
                var ind = 'dolar';
                caso2Promedio(ind).then(calcularPromedio).then(console.log);
				break;
			case '2':
                var ind = 'euro';
                caso2Promedio(ind).then(calcularPromedio).then(console.log);
				break;
			case '3':
                var ind = 'tasa_desempleo';
                caso2Promedio(ind).then(calcularPromedio).then(console.log);
                break;     
			case '4':
                menu();
				break;
			default:
				console.log('Opción no encontrada');
				caso2();
				break;
		}
}

const caso2Promedio = (ind) => {
    return new Promise((resolve, reject) =>{
        arreglo = [];
        var path='datos/';
        fs.readdir(path,(err,files)=>{
            files.forEach((file)=>{
                arreglo.push(getFile(path+file).then(JSON.parse).then((datos)=>{
                    for (var elem in datos){
                        if(elem==ind){
                            return Promise.resolve(datos[elem].valor);
                        }
                    }
                }).catch(console.error));
                escribeFil.getFile(path+file).then(JSON.parse).then((datos)=>{
                    return arregloFechas.push(datos['fecha']);
                }).catch(console.error);        
            });
            resolve(Promise.all(arreglo));
        });
    });  
}

const calcularPromedio = (resultado) => {
    return new Promise((resolve, reject)=>{
        arregloFechas.sort(ordenarFechas);
        var f1 = arregloFechas[0];
        var f2 = arregloFechas[arregloFechas.length-1];
        var suma=0;
        var cont=0;
        for(var i=0;i<resultado.length;i++){
            cont++;
            suma+=resultado[i];
        }
        resolve('El promedio es: '+(suma/cont)+' Rango de fecha: ['+f1+" - "+f2+']');
    });
}

const ordenarFechas = (a, b) => {
    var fa = a.split('-');
    var fb = b.split('-');
    if (fa[2]>fb[2]) {
      return 1;
    }else if(fa[2]==fb[2] && fa[1]>fb[1]){
        return 1;
    }else if(fa[2]==fb[2] && fa[1]==fb[1] && fa[0]>fb[0]){
        return 1;
    }
    if (fa[2]<fb[2]) {
        return -1;
    }else if(fa[2]==fb[2] && fa[1]<fb[1]){
        return -1;
    }else if(fa[2]==fb[2] && fa[1]==fb[1] && fa[0]<fb[0]){
      return -1;
    }
    return 0;
}

const caso3 = () =>{
    return new Promise((resolve, reject)=>{
        var path='datos/';
        fs.readdir(path,(err,files)=>{
                resolve(getFile(path+files[files.length-1]).then(JSON.parse).then((actual)));
        });      
    }); 
}

const actual = (datos)=>{
        var info='';
        var fecha = datos['fecha'];
        var hora = datos['hora'];
        var vDolar = datos['dolar'].valor;
        var vEuro = datos['euro'].valor;
        var vTasa_desempleo = datos['tasa_desempleo'].valor;
        info+='Fecha: '+fecha+', Hora: '+hora+', Valor Dolar: '+vDolar+', Valor Euro: '+vEuro+', Valor Tasa Desempleo: '+vTasa_desempleo;
        return Promise.resolve(info);
}

const caso4 = () => {
    return new Promise((resolve, reject) =>{
        var path='datos/';
        fs.readdir(path,(err,files)=>{
            files.forEach((file)=>{
                arregloValores.push(getFile(path+file).then(JSON.parse).then((datos)=>{
                    for (var elem in datos){
                        if(elem=='dolar'){
                            if(datos[elem].valor<minimoD){
                                minimoD=datos[elem].valor;
                            }
                        }
                        if(elem=='euro'){
                            if(datos[elem].valor<minimoE){
                                minimoE=datos[elem].valor;
                            }
                        }
                        if(elem=='tasa_desempleo'){
                            if(datos[elem].valor<minimoTD){
                                minimoTD=datos[elem].valor;
                            }
                            return Promise.resolve(arregloValores);
                        }
                    }
                }).catch(console.error));
                getFile(path+file).then(JSON.parse).then((datos)=>{
                    return arregloFechas2.push(datos['fecha']);
                }).catch(console.error);        
            });
            resolve(Promise.all(arregloValores));
        });
    });  
}

const calcularRangos = () => {
    return new Promise((resolve, reject)=>{
        arregloFechas2.sort(ordenarFechas2);
        var f1 = arregloFechas2[0];
        var f2 = arregloFechas2[arregloFechas2.length-1];
        resolve('Rango de fechas: ['+f1 +' - '+ f2+']\nEl valor mínimo del Dolar es: '+minimoD+'\nEl valor mínimo del Euro es: '+minimoE+'\nEl valor mínimo de la Tasa de Desempleo es: '+minimoTD);
    });
}

const ordenarFechas2 = (a, b) => {
    var fa = a.split('-');
    var fb = b.split('-');
    if (fa[2]>fb[2]) {
      return 1;
    }else if(fa[2]==fb[2] && fa[1]>fb[1]){
        return 1;
    }else if(fa[2]==fb[2] && fa[1]==fb[1] && fa[0]>fb[0]){
        return 1;
    }
    if (fa[2]<fb[2]) {
        return -1;
    }else if(fa[2]==fb[2] && fa[1]<fb[1]){
        return -1;
    }else if(fa[2]==fb[2] && fa[1]==fb[1] && fa[0]<fb[0]){
      return -1;
    }
    return 0;
  }

const caso5 = () => {
    return new Promise((resolve, reject) =>{
        var path='datos/';
        fs.readdir(path,(err,files)=>{
            files.forEach((file)=>{
                arregloValores2.push(getFile(path+file).then(JSON.parse).then((datos)=>{
                    for (var elem in datos){
                        if(elem=='dolar'){
                            if(datos[elem].valor>maximoD){
                                maximoD=datos[elem].valor;
                            }
                        }
                        if(elem=='euro'){
                            if(datos[elem].valor>maximoE){
                                maximoE=datos[elem].valor;
                            }
                        }
                        if(elem=='tasa_desempleo'){
                            if(datos[elem].valor>maximoTD){
                                maximoTD=datos[elem].valor;
                            }
                            return Promise.resolve(arregloValores2);
                        }
                    }
                }).catch(console.error));
                getFile(path+file).then(JSON.parse).then((datos)=>{
                    return arregloFechas3.push(datos['fecha']);
                }).catch(console.error);        
            });
            resolve(Promise.all(arregloValores2));
        });
    });  
}

const calcularRangos5 = () => {
    return new Promise((resolve, reject)=>{
        arregloFechas3.sort(ordenarFechas3);
        var f1 = arregloFechas3[0];
        var f2 = arregloFechas3[arregloFechas3.length-1];
        resolve('Rango de fechas: ['+f1 +' - '+ f2+']\nEl valor máximo del Dolar es: '+maximoD+'\nEl valor máximo del Euro es: '+maximoE+'\nEl valor máximo de la Tasa de Desempleo es: '+maximoTD);
    });
}

const ordenarFechas3 = (a, b) => {
    var fa = a.split('-');
    var fb = b.split('-');
    if (fa[2]>fb[2]) {
      return 1;
    }else if(fa[2]==fb[2] && fa[1]>fb[1]){
        return 1;
    }else if(fa[2]==fb[2] && fa[1]==fb[1] && fa[0]>fb[0]){
        return 1;
    }
    if (fa[2]<fb[2]) {
        return -1;
    }else if(fa[2]==fb[2] && fa[1]<fb[1]){
        return -1;
    }else if(fa[2]==fb[2] && fa[1]==fb[1] && fa[0]<fb[0]){
      return -1;
    }
    return 0;
  }


const getFile = fileName => {
    return new Promise((resolve,reject)=>{
        fs.readFile(fileName,(err,data)=>{
            if(err){
                reject(err);
                return;
            }
            resolve(data);
        });
    });
}

/**************************************************/

/*//Utilizado para consumir desde la URL web
function caso1(){
    fetch('https://mindicador.cl/api').then(status).then(obtenerJson).then(filtrado).then((datos)=>{
		fs.writeFile("datos/"+String(timestamp('HH-mm-ss'))+'.ind',JSON.stringify(datos),(err)=>{
		console.error(err)}); 
	}).catch(console.error);
}


const rangoFechas = (ind) => {
    return new Promise((resolve, reject)=>{
        arregloFechas = [];
        var path='../EvaluacionNode/datos/';
        fs.readdir(path,(err,files)=>{
            files.forEach((file)=>{
                arregloFechas.push(getFile(path+file).then(JSON.parse).then((datos)=>{
                    return Promise.resolve(datos['fecha']);
                }).catch(console.error));        
            });
            arregloFechas.sort(compararFechas);
            resolve(Promise.all(arregloFechas));
        });
    });  
}

function caso1(){
    var n=0;
    var path='../EvaluacionNode/datos/';
    fs.readdir(path,(err,files)=>{
        n=files.length+1;
    });

    getFile('../EvaluacionNode/otros/api.json').then(filtrado).then((datos)=>{
		fs.writeFile("datos/"+n+String(timestamp('_YYYY-MM-DD-HH-mm-ss'))+'.ind',JSON.stringify(datos),(err)=>{
            console.error(err)});
    }).catch(console.error);
}

const filtrado = (data) => {
    var datos = JSON.parse(data);

*/